//have add top view programatically and bottom view as custom view just to show both variety

import UIKit

class FullScreenViewController: UIViewController {

    @IBOutlet weak var bottomView: BottomView!
    @IBOutlet weak var fullScreenImageView: UIImageView!
    
    public var photo: Photo?
    private let topView = UIView();

    override func viewDidLoad() {
        super.viewDidLoad()
        
        fullScreenImageView.kf.setImage(with: URL(string: photo?.image_url[1] ?? "" ))
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(FullScreenViewController.tapDetected))
        fullScreenImageView.isUserInteractionEnabled = true
        fullScreenImageView.addGestureRecognizer(singleTap)
        
       
        bottomView.name.text = photo?.name
        bottomView.userName.text = photo?.user.username
        fullScreenImageView.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin]
        fullScreenImageView.contentMode = .scaleAspectFit // OR .scaleAspectFill
        fullScreenImageView.clipsToBounds = true
        addTopView()
        
    }
    
    override func  viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        DispatchQueue.main.async {
             self.addTopView()
        }
    }
    
    private func addTopView() {
        let image = UIImage(named: "icons8-delete-50") as UIImage?
        
        let cancelButton = UIButton(type: .custom)
        cancelButton.frame = CGRect(x: 20, y: 40, width: 40, height: 40)
        cancelButton.setImage(image, for: .normal)
        cancelButton.addTarget(self, action: #selector(cancelAction), for: .touchUpInside)

        topView.addSubview(cancelButton)
        topView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100)
        topView.backgroundColor = .gray
        topView.alpha = 0.5
        view.addSubview(topView)
    }
    
    @objc func cancelAction(sender: UIButton!) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func tapDetected() {
        
        if topView.isHidden {
            bottomView.isHidden = false
            topView.isHidden = false
        } else {
            topView.isHidden = true
            bottomView.isHidden = true
        }
    }
}
