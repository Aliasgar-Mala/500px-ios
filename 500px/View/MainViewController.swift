import UIKit
import Kingfisher

class MainViewController: UIViewController {
    
    @IBOutlet private var viewModel: PhotoViewModel!
    @IBOutlet weak var collectionView: UICollectionView!
    private let numberOfItemsOnEachRow: CGFloat = 2
    private let identifier = String(describing: PhotoCell.self)
    

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib(nibName: identifier, bundle: nil), forCellWithReuseIdentifier: identifier)
        fetchPhotos()
        
    }
    
    
    private func fetchPhotos() {
        viewModel.makeCall() { [unowned self]  in
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
}

extension MainViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.totalCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! PhotoCell
        
        if isLoadingCell(for: indexPath) {
            cell.configure(with: .none)
        } else {
            cell.configure(with: viewModel.photo(at: indexPath.item))
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let fullScreenViewController = storyBoard.instantiateViewController(withIdentifier: String(describing: FullScreenViewController.self)) as! FullScreenViewController
        fullScreenViewController.photo = viewModel.photo(at: indexPath.item)
        self.present(fullScreenViewController, animated:true, completion:nil)
    }
}

extension MainViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.frame.width/numberOfItemsOnEachRow
        let yourHeight = yourWidth
        
        return CGSize(width: yourWidth, height: yourHeight)
    }
}

//MARK: paging
extension MainViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        if indexPaths.contains(where: isLoadingCell) {
            fetchPhotos()
        }
    }
}

//MARK: helper methods
private extension MainViewController {
    func isLoadingCell(for indexPath: IndexPath) -> Bool {
        return indexPath.item >= viewModel.currentCount
    }
}
