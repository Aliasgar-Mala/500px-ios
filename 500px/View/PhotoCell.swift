import UIKit
import Kingfisher

class PhotoCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    
    public func configure(with photo: Photo?) {
        self.photoImageView.kf.setImage(with: URL(string: photo?.image_url.first ?? "" ))
    }
}
