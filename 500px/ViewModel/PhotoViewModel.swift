import UIKit

class PhotoViewModel: NSObject {
    
    let service: Service  = Service()
    private var photos: [Photo] = []
    private var isFetchInProgress = false
    private var currentPage = 1
    private var total = 0
    
    var currentCount: Int {
        return photos.count
    }
    
    var totalCount: Int {
        return total
    }
    
    public func makeCall( completion: @escaping () -> ()) {
        
        guard !isFetchInProgress else {
            return
        }
        
        isFetchInProgress = true
        service.getPhotos(pageNumber:currentPage) {[unowned self] (photoResponse, error) in
            
            guard let response = photoResponse else {
                return
            }
            self.photos.append(contentsOf: response.photos)
            self.currentPage += 1
            self.isFetchInProgress = false
            self.total = response.total_items
            completion()
        }
    }
    
    func photo(at index: Int) -> Photo {
        return photos[index]
    }
}
