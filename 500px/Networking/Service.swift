import UIKit

class Service {
    var keys: NSDictionary = [:]
    
    init() {
        if let path = Bundle.main.path(forResource: "Keys", ofType: "plist") {
            keys = NSDictionary(contentsOfFile: path) ?? [:]
        }
    }
    public func getPhotos(pageNumber: Int, taskCallback: @escaping (PhotoResponse?, Error?) -> ()) {
        
        let URLString = "https://api.500px.com/v1/photos"
        
        var components = URLComponents(string: URLString)!
        
        guard let consumer_key = keys["consumer_key"] as? String else {
            return
        }
        components.queryItems = ["consumer_key": consumer_key,
                                 "feature":"popular",
                                 "image_size":"3,4",
                                 "page":"\(pageNumber)"].map { (key, value) in
                                    URLQueryItem(name: key, value: value)
        }
        let request = URLRequest(url: components.url!)
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) -> Void in
            if let data = data {
                let json = try? JSONDecoder().decode(PhotoResponse.self, from: data)
                if let response = response as? HTTPURLResponse , 200...299 ~= response.statusCode {
                    taskCallback(json, nil)
                } else {
                    taskCallback(nil, error)
                }
            }
        })
        task.resume()
    }
    
    
    
   
    
    
}
