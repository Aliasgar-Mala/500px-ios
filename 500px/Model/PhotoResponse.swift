import Foundation

//can use coding keys to give camelCase name to properties
struct PhotoResponse: Decodable {
    let total_pages: Int
    let total_items: Int
    let photos: [Photo]
    let current_page: Int
}

struct Photo: Decodable {
    let name: String
    let image_url: [String]
    let user: User
}

struct User: Decodable {
    let username: String
}
